package pk.birdie;

import junit.framework.TestCase;

import java.util.Calendar;
import java.util.GregorianCalendar;

import pk.birdie.utilities.DateFormatter;

public class DateFormatterTests extends TestCase {

    public DateFormatterTests(String name) {
        super(name);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public final void testSuccessCase() {
        GregorianCalendar calendar = new GregorianCalendar(2015, Calendar.JANUARY, 1, 0, 0, 0);
        String output = DateFormatter.dateToString(calendar.getTime());
        assertEquals("01/01, 00:00:00", output);
    }

    public final void testFailure() {
        try {
            DateFormatter.dateToString(null);
        } catch (NullPointerException e) {
            assertTrue(true);
            return;
        }
        assertTrue(false);
    }
}
