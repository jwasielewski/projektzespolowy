package pk.birdie.profile;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pk.birdie.R;
import twitter4j.User;

/**
 * Layout wyświetlający nazwę użytkownika oraz zdjęcie w tle
 */
public class  ProfileDetailsLayout extends RelativeLayout {
    private static final Logger log = LoggerFactory.getLogger(ProfileDetailsLayout.class);

    private ImageView background;
    private TextView description;

    public ProfileDetailsLayout(Context context) {
        super(context);

        init();
    }

    public ProfileDetailsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public ProfileDetailsLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    /**
     * Wstrzykiwanie layoutu i pobieranie elementów
     */
    private void init() {
        View.inflate(getContext(), R.layout.profile_details, this);

        background = (ImageView) findViewById(R.id.profile_background);
        description = (TextView) findViewById(R.id.profile_user_description);
    }

    /**
     * Layout uzupełniany jest o dane przekazanego użytkownika
     * @param user obiekt użytkownika do wyświetlenia
     */
    public void showUserDetails(User user) {
        Picasso.with(getContext()).load(user.getProfileBannerRetinaURL()).fit().centerCrop().noFade().into(background);

        setBackgroundColor(Color.parseColor("#" + user.getProfileBackgroundColor()));
        description.setTextColor(Color.parseColor("#" + user.getProfileTextColor()));

        description.setText(user.getDescription());
    }
}
