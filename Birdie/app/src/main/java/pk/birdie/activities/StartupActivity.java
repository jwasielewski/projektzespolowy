package pk.birdie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.pixplicity.easyprefs.library.Prefs;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Pierwsza aktywność uruchamiana przy starcie aplikacji.
 * Jej zadaniem jest sprawdzenie czy użtykownik jest już zalogowany. Jeśli nie to przekierowywuje go do widoku
 * logowania. Natomiast jeśli użytkownik wcześniej już się zalogował to zostanie przekierowany do HomeActivity.
 * Dodatkowo pobierane są klucze do autoryzacji i tworzony jest kontekst dla biblioteki Twitter4J.
 */
public class StartupActivity extends ActionBarActivity {

    public static final String KEY_IS_USER_LOGGED = "KEY_IS_USER_LOGGED";
    public static final String KEY_TOKEN = "KEY_TOKEN";
    public static final String KEY_TOKEN_SECRET = "KEY_TOKEN_SECRET";
    public static final String KEY_USER_ID = "KEY_USER_ID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        Prefs.initPrefs(this);

        if (Prefs.getBoolean(KEY_IS_USER_LOGGED, false)) {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.setDebugEnabled(false)
                    .setOAuthConsumerKey(Configuration.CONSUMER_KEY)
                    .setOAuthConsumerSecret(Configuration.CONSUMER_SERCRET)
                    .setOAuthAccessToken(Prefs.getString(KEY_TOKEN, ""))
                    .setOAuthAccessTokenSecret(Prefs.getString(KEY_TOKEN_SECRET, ""));
            Configuration.setConfigurationContext(configurationBuilder.build());

            Configuration.setCurrentLoggedUserId(Prefs.getLong(KEY_USER_ID, -1));

            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);

            finish();

        } else {
            Button login = (Button) findViewById(R.id.startup_login_button);
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StartupActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_startup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
