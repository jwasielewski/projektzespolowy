package pk.birdie.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import pk.birdie.statuseslistview.StatusesListView;
import pk.birdie.statuseslistview.StatusesListener;
import pk.birdie.utilities.KeyboardUtils;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Aktywaność pokazująca użytkownikowy statusy wyszukane na podstawie przekazanej jej frazie.
 * <code>Intent intent = new Intent(context, SearchActivity.class);
 * intent.putExtra(SearchActivity.SEARCH_PHARSE, pharse);
 * context.startActivity(intent);
 * </code>
 */
public class SearchActivity extends ActionBarActivity implements StatusesListener {
    private static final Logger log = LoggerFactory.getLogger(SearchActivity.class);

    public static final String SEARCH_PHARSE = "SEARCH_PHARSE";

    private EditText searchQuery;
    private StatusesListView statusesListView;

    private Twitter twitter;
    private Query query;
    private QueryResult queryResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        statusesListView = (StatusesListView) findViewById(R.id.search_statuses);
        statusesListView.setStatusesListener(this);
        statusesListView.hideFooter();

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);

            actionBar.setCustomView(R.layout.actionbar_search);
            searchQuery = (EditText) actionBar.getCustomView().findViewById(R.id.actionbar_search);
            searchQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        KeyboardUtils.hide(SearchActivity.this, searchQuery);
                        performSearch();
                        return true;
                    }
                    return false;
                }
            });

            Toolbar parent =(Toolbar) actionBar.getCustomView().getParent();
            parent.setContentInsetsAbsolute(0, 0);
        }

        twitter = new TwitterFactory(Configuration.getConfigurationContext()).getInstance();

        if (getIntent() != null) {
            if (getIntent().getStringExtra(SEARCH_PHARSE) != null) {
                searchQuery.setText(getIntent().getStringExtra(SEARCH_PHARSE));
                performSearch();
            }
        }
    }

    /**
     * Funkcja uruchamiająca wyszukiwanie jeśli użytkownik wpisał jakąkolwiek frazę.
     * Jeśli adapter zawierał już dane z poprzedniego wyszukiwania to są one usuwane.
     */
    private void performSearch() {
        String queryText = searchQuery.getText().toString();
        if (queryText.length() > 0) {
            if (statusesListView.getAdapter().getCount() > 0) {
                statusesListView.clear();
            }

            query = new Query(queryText);
            getNextPageOfSearchResults();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserNameClick(User user) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(ProfileActivity.PROFILE_ID, user.getId());
        startActivity(intent);
    }

    @Override
    public void onPageEnd() {
        log.debug("onPageEnd()");

        if (query != null) {
            getNextPageOfSearchResults();
        }
    }

    @Override
    public void onStatusClick(final Status status) {
        log.trace("onStatucClick('{}')", status.getId());

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.do_you_want_to_share_this_status))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retweet(status.getId());
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ...
                    }
                });

        builder.create().show();
    }

    /**
     * Pomocnicza funckja uruchamiająca nowy wątek, w którym udostępniany jest status wskazany przez użytkownika
     * @param id statusu
     */
    private void retweet(final long id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    twitter.retweetStatus(id);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, getResources().getString(R.string.shared),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (final TwitterException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SearchActivity.this, e.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    /**
     * Funkcja pobierająca następną stronę statusów
     */
    private void getNextPageOfSearchResults() {
        statusesListView.setBlockFireListenerOnPageEnd(true);
        statusesListView.showFooter();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (queryResult == null) {
                        queryResult = twitter.search(query);
                    } else {
                        query = queryResult.nextQuery();

                        if (query != null) {
                            queryResult = twitter.search(query);
                        } else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    statusesListView.setBlockFireListenerOnPageEnd(true);
                                    statusesListView.hideFooter();
                                }
                            });
                            return;
                        }
                    }

                    final List<Status> statuses = queryResult.getTweets();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // statuses == null jeśli jest to ostatnia strona wyszukiwania
                            statusesListView.setBlockFireListenerOnPageEnd(statuses == null);
                            statusesListView.hideFooter();
                            if (statuses != null) {
                                statusesListView.addStatuses(statuses);
                            }
                        }
                    });
                } catch (final TwitterException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            Toast.makeText(SearchActivity.this, e.getErrorMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();
    }
}
