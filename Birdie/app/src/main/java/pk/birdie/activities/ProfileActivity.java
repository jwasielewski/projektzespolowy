package pk.birdie.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import pk.birdie.profile.ProfileDetailsLayout;
import pk.birdie.statuseslistview.StatusesListView;
import pk.birdie.statuseslistview.StatusesListener;
import pk.birdie.utilities.StatusesListHelper;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Klasa wyświetlająca profil uzytkownika na podstawie przekazanego jej ID
 * <code>Intent intent = new Intent(context, ProfileActivity.class);
 * intent.putLongExtra(ProfileActivity.PROFILE_ID, id);
 * context.startActivity(intent);</code>
 * Jeśli użytkownik o podanym id nie istnieje to aktywność wyświetli odpowiedni komunikat w Toast i zakończy
 * swoje działanie.
 */
public class ProfileActivity extends ActionBarActivity implements StatusesListener {
    private static final Logger log = LoggerFactory.getLogger(ProfileActivity.class);

    public static final String PROFILE_ID = "PROFILE_ID";

    private static final int STATUSES_PAGE_LIMIT = 20;

    private ProfileDetailsLayout details;
    private StatusesListView statuses;
    private ProgressBar progressBar;

    private long userId;
    private Twitter twitter;
    private Paging paging;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        progressBar = (ProgressBar) findViewById(R.id.user_progressbar);
        details = (ProfileDetailsLayout) findViewById(R.id.user_details);
        statuses = (StatusesListView) findViewById(R.id.user_statuses);
        statuses.setStatusesListener(this);

        twitter = new TwitterFactory(Configuration.getConfigurationContext()).getInstance();

        paging = new Paging();
        paging.setCount(STATUSES_PAGE_LIMIT);
        paging.setPage(1);

        Intent intent = getIntent();
        if (intent == null) {
            Toast.makeText(this, "Internal error", Toast.LENGTH_SHORT).show();
        } else if (intent.getLongExtra(PROFILE_ID, -1) == -1) {
            Toast.makeText(this, "Internal error", Toast.LENGTH_SHORT).show();
        } else if (intent.getLongExtra(PROFILE_ID, -1) != -1) {
            userId = intent.getLongExtra(PROFILE_ID, -1);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final User user = twitter.showUser(userId);
                        userId = user.getId();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showLayout(user);
                            }
                        });
                    } catch (final TwitterException e) {
                        log.error(e.getErrorMessage());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(ProfileActivity.this, e.getErrorMessage(), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        });
                    }
                }
            }).start();
        } else {
            Toast.makeText(this, "Internal error", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    /**
     * Pomocnicza funckja wyświetlająca i wypełniająca danymi layout na podstawie obiektu pobranego użytkownika.
     * @param user dane użytkownika do wyświetlenia
     */
    private void showLayout(User user) {
        progressBar.setVisibility(View.GONE);
        details.setVisibility(View.VISIBLE);
        statuses.setVisibility(View.VISIBLE);

        setTitle(user.getName());

        details.showUserDetails(user);
        StatusesListHelper.getNextPageOfUserStatuses(user.getId(), this, twitter, statuses, paging);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserNameClick(User user) {

    }

    @Override
    public void onPageEnd() {
        StatusesListHelper.getNextPageOfUserStatuses(userId, this, twitter, statuses, paging);
    }

    @Override
    public void onStatusClick(final Status status) {
        log.trace("onStatucClick('{}')", status.getId());

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.do_you_want_to_share_this_status))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retweet(status.getId());
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ...
                    }
                });

        builder.create().show();
    }

    private void retweet(final long id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    twitter.retweetStatus(id);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ProfileActivity.this, getResources().getString(R.string.shared),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (final TwitterException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ProfileActivity.this, e.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }
}
