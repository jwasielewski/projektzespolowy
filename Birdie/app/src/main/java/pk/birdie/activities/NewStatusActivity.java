package pk.birdie.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pk.birdie.configuration.Configuration;
import pk.birdie.utilities.RoundedTransformation;
import twitter4j.TwitterException;
import twitter4j.User;
import pk.birdie.R;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

/**
 * Aktuwność pozwalająca użytkownikowi na wprowadzenie oraz wysłanie nowego statusu.
 */
public class NewStatusActivity extends ActionBarActivity {
    private static final Logger log = LoggerFactory.getLogger(NewStatusActivity.class);
    private ImageView avatar;
    private EditText message;
    private TextView wordCount;
    private Twitter twitter;
    private TextView userName;
    private TextView userNick;
    private int lenght;

    private final TextWatcher mTextEditorWatcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //This sets a textview to the current length
            lenght = s.length();
            wordCount.setText(lenght+"/140");
        }

        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        lenght = 0;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_status);

        avatar = (ImageView) findViewById(R.id.avatar);
        message = (EditText) findViewById(R.id.message);
        wordCount = (TextView) findViewById(R.id.wordCount);
        userName = (TextView) findViewById(R.id.userName);
        userNick = (TextView) findViewById(R.id.userNick);

        message.addTextChangedListener(mTextEditorWatcher);


        twitter = new TwitterFactory(Configuration.getConfigurationContext()).getInstance();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final User user = twitter.showUser(Configuration.getCurrentLoggedUserId());
                    final String name = user.getName();
                    final String nick = user.getScreenName();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.with(NewStatusActivity.this).load(user.getProfileImageURL()).transform(new RoundedTransformation(8,2)).noFade().into(avatar);
                            userName.setText(name);
                            userNick.setText("@" + nick);

                        }
                    });

                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_status, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.send_status) {
            if (lenght > 140) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(NewStatusActivity.this, "Za dluga wiadomosc!", Toast.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        twitter.updateStatus(message.getText().toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(NewStatusActivity.this, "Sukces!", Toast.LENGTH_SHORT).show();
                            }
                        });
                        finish();
                    } catch (final Throwable t) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(NewStatusActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }).start();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }}

