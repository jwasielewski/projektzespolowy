package pk.birdie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import pk.birdie.utilities.TwitterOAuthView;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Klasa obsługująca logowanie do Twittera za pomocą zmodyfikowanej kontrolki WebView
 */
public class LoginActivity extends ActionBarActivity implements TwitterOAuthView.Listener {
    private static final Logger log = LoggerFactory.getLogger(LoginActivity.class);

    private FrameLayout loginLayout;
    private TwitterOAuthView oAuthView;
    private boolean oAuthStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginLayout = (FrameLayout) findViewById(R.id.login_layout);
        oAuthView = new TwitterOAuthView(this);
        loginLayout.addView(oAuthView);

        oAuthStarted = false;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (oAuthStarted) {
            return;
        }

        oAuthStarted = true;

        oAuthView.start(Configuration.CONSUMER_KEY, Configuration.CONSUMER_SERCRET, Configuration.CALLBACK_URL,
                true, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccess(final TwitterOAuthView view, final AccessToken accessToken) {
        view.setVisibility(View.GONE);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Prefs.putBoolean(StartupActivity.KEY_IS_USER_LOGGED, true);
                Prefs.putString(StartupActivity.KEY_TOKEN, accessToken.getToken());
                Prefs.putString(StartupActivity.KEY_TOKEN_SECRET, accessToken.getTokenSecret());

                ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
                configurationBuilder.setDebugEnabled(false)
                        .setOAuthConsumerKey(Configuration.CONSUMER_KEY)
                        .setOAuthConsumerSecret(Configuration.CONSUMER_SERCRET)
                        .setOAuthAccessToken(accessToken.getToken())
                        .setOAuthAccessTokenSecret(accessToken.getTokenSecret());
                Configuration.setConfigurationContext(configurationBuilder.build());

                Twitter twitter = new TwitterFactory(Configuration.getConfigurationContext()).getInstance();

                try {
                    long id = twitter.getId();
                    Configuration.setCurrentLoggedUserId(id);
                    Prefs.putLong(StartupActivity.KEY_USER_ID, id);
                } catch (TwitterException e) {
                    log.error(e.getErrorMessage());
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }).start();
    }

    @Override
    public void onFailure(TwitterOAuthView view, TwitterOAuthView.Result result) {
        Toast.makeText(this, result + "", Toast.LENGTH_SHORT).show();
    }
}
