package pk.birdie.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import pk.birdie.statuseslistview.StatusesListView;
import pk.birdie.statuseslistview.StatusesListener;
import pk.birdie.utilities.StatusesListHelper;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 * Klasa pokazująca zalogowanemu użytkownikowi statusy obserwowanych użytkowników.
 */
public class HomeActivity extends ActionBarActivity implements StatusesListener, SwipeRefreshLayout.OnRefreshListener {
    private static final Logger log = LoggerFactory.getLogger(HomeActivity.class);

    private static final int STATUSES_PAGE_LIMIT = 20;

    private Twitter twitter;
    private SwipeRefreshLayout swipeLayout;
    private StatusesListView statusesListView;
    private Paging paging;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.home_swipe_layout);
        swipeLayout.setColorSchemeColors(getResources().getColor(R.color.accent));
        swipeLayout.setOnRefreshListener(this);

        statusesListView = (StatusesListView) findViewById(R.id.home_statuses);
        statusesListView.setStatusesListener(this);

        paging = new Paging();
        paging.setCount(STATUSES_PAGE_LIMIT);
        paging.setPage(1);

        twitter = new TwitterFactory(Configuration.getConfigurationContext()).getInstance();

        StatusesListHelper.getNextPageOfHomeTimeline(this, twitter, statusesListView, paging);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_new_status) {
            Intent intent = new Intent(this, NewStatusActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_show_my_profile) {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra(ProfileActivity.PROFILE_ID, Configuration.getCurrentLoggedUserId());
            startActivity(intent);
            return true;
        } else if (id == R.id.action_logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.do_you_want_to_logout));
            builder.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Prefs.putBoolean(StartupActivity.KEY_IS_USER_LOGGED, false);
                    Prefs.putLong(StartupActivity.KEY_USER_ID, -1);
                    Prefs.getString(StartupActivity.KEY_TOKEN, "");
                    Prefs.getString(StartupActivity.KEY_TOKEN_SECRET, "");

                    Intent intent = new Intent(HomeActivity.this, StartupActivity.class);
                    startActivity(intent);

                    finish();
                }
            });
            builder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create().show();
            return true;
        } else if (id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onUserNameClick(User user) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra(ProfileActivity.PROFILE_ID, user.getId());
        startActivity(intent);
    }

    @Override
    public void onPageEnd() {
        log.trace("onPageEnd()");

        StatusesListHelper.getNextPageOfHomeTimeline(this, twitter, statusesListView, paging);
    }

    @Override
    public void onStatusClick(final Status status) {
        log.trace("onStatucClick('{}')", status.getId());

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.do_you_want_to_share_this_status))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retweet(status.getId());
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ...
                    }
                });

        builder.create().show();
    }

    /**
     * Pomocnicza funckja uruchamiająca nowy wątek, w którym udostępniany jest status wskazany przez użytkownika
     * @param id statusu
     */
    private void retweet(final long id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    twitter.retweetStatus(id);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HomeActivity.this, getResources().getString(R.string.shared),
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (final TwitterException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HomeActivity.this, e.getErrorMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    @Override
    public void onRefresh() {
        log.debug("onRefresh()");

        statusesListView.setBlockFireListenerOnPageEnd(true);
        statusesListView.showFooter();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Paging refreshPaging = new Paging();
                    paging.setSinceId(statusesListView.getStatuses().get(0).getId());
                    final List<Status> homeStatuses = twitter.getHomeTimeline(refreshPaging);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            statusesListView.addStatusesAtBeginning(homeStatuses);
                            swipeLayout.setRefreshing(false);
                        }
                    });
                } catch (final TwitterException e) {
                    e.printStackTrace();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            Toast.makeText(HomeActivity.this, e.getErrorMessage(), Toast.LENGTH_LONG).show();
                            swipeLayout.setRefreshing(false);
                        }
                    });
                }
            }
        }).start();
    }
}
