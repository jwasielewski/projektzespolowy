package pk.birdie.utilities;

import android.app.Activity;
import android.widget.Toast;

import java.util.List;

import pk.birdie.statuseslistview.StatusesListView;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

/**
 * Klasa pomocnicza pobierająca statusy
 */
public class StatusesListHelper {

    private StatusesListHelper() {

    }

    /**
     * Funckja pobierająca statusy obserwowanych profili
     * @param activity kontekst
     * @param twitter obeikt biblioteki
     * @param statusesListView lista statusów
     * @param paging stronnicowanie
     */
    public static void getNextPageOfHomeTimeline(final Activity activity, final Twitter twitter,
                                                 final StatusesListView statusesListView, final Paging paging) {

        statusesListView.setBlockFireListenerOnPageEnd(true);
        statusesListView.showFooter();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<Status> homeStatuses = twitter.getHomeTimeline(paging);

                    PagingHelper.setNextPage(paging, homeStatuses);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            statusesListView.addStatuses(homeStatuses);
                        }
                    });
                } catch (final TwitterException e) {
                    e.printStackTrace();

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            Toast.makeText(activity, e.getErrorMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();

    }

    /**
     * Funckja pobierająca statusy konkretnego użytkownika
     * @param userId id użytkownika
     * @param activity kontekst
     * @param twitter obiekt biblioteki
     * @param statusesListView lista statusów
     * @param paging stronnicowanie
     */
    public static void getNextPageOfUserStatuses(final long userId, final Activity activity, final Twitter twitter,
                                                 final StatusesListView statusesListView, final Paging paging) {

        statusesListView.setBlockFireListenerOnPageEnd(true);
        statusesListView.showFooter();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<Status> statuses = twitter.getUserTimeline(userId, paging);

                    PagingHelper.setNextPage(paging, statuses);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            statusesListView.addStatuses(statuses);
                        }
                    });
                } catch (final TwitterException e) {
                    e.printStackTrace();

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            statusesListView.setBlockFireListenerOnPageEnd(false);
                            statusesListView.hideFooter();
                            Toast.makeText(activity, e.getErrorMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();
    }

}
