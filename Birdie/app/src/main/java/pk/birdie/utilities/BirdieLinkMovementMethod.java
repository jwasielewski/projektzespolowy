package pk.birdie.utilities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Layout;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.widget.TextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pk.birdie.activities.ProfileActivity;
import pk.birdie.activities.SearchActivity;

/**
 * Klasa obsługująca klikanie w linki, hashtagi oraz oznaczenia użytkowników w statsuach
 */
public class BirdieLinkMovementMethod extends LinkMovementMethod {
    private static final Logger log = LoggerFactory.getLogger(BirdieLinkMovementMethod.class);

    private static Context context;
    private static BirdieLinkMovementMethod movementMethod = new BirdieLinkMovementMethod();

    @Override
    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
        int action = event.getAction();

        if (action == MotionEvent.ACTION_UP) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();

            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            URLSpan[] link = buffer.getSpans(off, off, URLSpan.class);
            if (link.length != 0) {
                String url = link[0].getURL();

                if (url.startsWith("http://") || url.startsWith("https://")) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    context.startActivity(intent);
                } else if (url.startsWith("@")) {
                    long id = Long.valueOf(url.substring(1));
                    Intent intent = new Intent(context, ProfileActivity.class);
                    intent.putExtra(ProfileActivity.PROFILE_ID, id);
                    context.startActivity(intent);
                } else if (url.startsWith("#")) {
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra(SearchActivity.SEARCH_PHARSE, url);
                    context.startActivity(intent);
                }

                return true;
            }
        }

        return super.onTouchEvent(widget, buffer, event);
    }

    public static MovementMethod getInstance(Context context) {
        BirdieLinkMovementMethod.context = context;
        return movementMethod;
    }

}
