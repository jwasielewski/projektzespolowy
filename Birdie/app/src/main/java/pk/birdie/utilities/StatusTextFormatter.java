package pk.birdie.utilities;

import android.text.Html;
import android.text.Spanned;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.URLEntity;
import twitter4j.UserMentionEntity;

public class StatusTextFormatter {
    private static final Logger log = LoggerFactory.getLogger(StatusTextFormatter.class);

    private StatusTextFormatter() {

    }

    public static Spanned format(Status status) {
        String text = status.getText();

        for (URLEntity entity : status.getURLEntities()) {
            text = text.replaceAll(entity.getURL(),
                    String.format("<a href='%s'>%s</a>", entity.getURL(), entity.getDisplayURL()));
        }

        for (UserMentionEntity entity : status.getUserMentionEntities()) {
            text = text.replaceAll("@" + entity.getScreenName(),
                    String.format("<a href='@%s'>@%s</a>", entity.getId(), entity.getScreenName()));
        }

        for (HashtagEntity entity : status.getHashtagEntities()) {
            text = text.replaceAll("#" + entity.getText(),
                    String.format("<a href='#%s'>#%s</a>", entity.getText(), entity.getText()));
        }

        int lastIndex;
        if ((lastIndex = text.lastIndexOf("http://t.co/")) != -1) {
            if (text.indexOf(">", lastIndex) == -1) {
                text = text.substring(0, lastIndex);
            }
        }

        return Html.fromHtml(text);
    }
}
