package pk.birdie.utilities;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Klasa zawierająca wygodny interfejs do manipulowania klawiaturą
 */
public class KeyboardUtils {

    private KeyboardUtils() {

    }

    /**
     * Funkcja chowająca klawiaturę
     * @param context kontekst
     * @param view kontrolka, do której "pisze" klawiatura
     */
    public static void hide(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
