package pk.birdie.utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Klasa formatująca datę
 */
public class DateFormatter {

    private static final String DATE_FORMAT = "dd/MM, HH:mm:ss";

    private DateFormatter() {

    }

    public static String dateToString(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT, new Locale("pl", "PL"));
        return format.format(date);
    }
}
