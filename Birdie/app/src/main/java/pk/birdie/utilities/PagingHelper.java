package pk.birdie.utilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import twitter4j.Paging;
import twitter4j.Status;

/**
 * Klasa ułatwiająca ustawienie kolejnej strony pobierania
 */
public class PagingHelper {
    static final Logger log = LoggerFactory.getLogger(PagingHelper.class);

    private PagingHelper() {

    }

    /**
     * Funcka ustawiająca kolejną stronę pobierania na podstawie ostatniego statusu
     * @param paging obiekt zawierający stronnicowanie
     * @param statuses lista statusów
     */
    public static void setNextPage(Paging paging, List<Status> statuses) {
        // zaczynamy ściągać strona po stronie od ostatniego statusu
        // wyeliminuje to ewentualne duplikaty w przypadku gdy w między czasie
        // zostanie dodany nowy status
        if (statuses.size() > 0) {
            paging.setMaxId(statuses.get(statuses.size() - 1).getId());
        } else {
            log.error("statuses.size() = 0 !!");
        }

    }

}
