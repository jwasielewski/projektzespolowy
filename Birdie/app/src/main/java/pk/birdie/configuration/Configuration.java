package pk.birdie.configuration;

/**
 * Klasa przechowująca konfigurację
 */
public class Configuration {

    private Configuration() {

    }

    /**
     * Klucz aplikacji
     */
    public static final String CONSUMER_KEY = "XG6Lv5PmszV3wk2yDMxZpCwaq";

    /**
     * Kolejny klucz aplikacji
     */
    public static final String CONSUMER_SERCRET = "hlTreVgYuDEEfnuSFvRJpA86JW5wGmBqjTO0LnnFn7jHg8EjfC";

    /**
     * Adres zwrotny, pomaga on pozyskać klucze zalogowanego użytkownika kontrolce TwitterOAuthView
     */
    public static final String CALLBACK_URL = "http://jwasielewski.pl";

    /**
     * Konfiguracja biblioteki Twitter4J
     */
    private static twitter4j.conf.Configuration configurationContext;

    public static twitter4j.conf.Configuration getConfigurationContext() {
        return configurationContext;
    }

    public static void setConfigurationContext(twitter4j.conf.Configuration configurationContext) {
        Configuration.configurationContext = configurationContext;
    }

    /**
     * Id aktualnie zalogowanego użytkownika
     */
    private static long currentLoggedUserId = -1;

    public static long getCurrentLoggedUserId() {
        return currentLoggedUserId;
    }

    public static void setCurrentLoggedUserId(long currentLoggedUserId) {
        Configuration.currentLoggedUserId = currentLoggedUserId;
    }
}
