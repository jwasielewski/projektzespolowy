package pk.birdie.statuseslistview;

import twitter4j.Status;
import twitter4j.User;

/**
 * Interfejs służący do otrzymywania zdarzeń z {@link pk.birdie.statuseslistview.StatusesListView}
 */
public interface StatusesListener {

    /**
     * Zdarzenie naciśnięcia nazwy użytkownika
     * @param user obiekt reprezentujący danego użytkownika
     */
    void onUserNameClick(User user);

    /**
     * Zdarzenie informujące, że użytkownik znalazł się na końcu listy
     */
    void onPageEnd();

    /**
     * Zdarzenie informujące o naciśniętym statusie
     * @param status naciśnięty status
     */
    void onStatusClick(Status status);

}
