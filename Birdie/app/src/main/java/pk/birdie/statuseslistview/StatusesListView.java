package pk.birdie.statuseslistview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import pk.birdie.R;
import twitter4j.Status;

/**
 * Klasa dziedzicząca po {@link android.widget.ListView}, renderująca przekazane jej statusy
 */
public class StatusesListView extends ListView implements AbsListView.OnScrollListener {
    private static final Logger log = LoggerFactory.getLogger(StatusesListView.class);

    private StatusesAdapter adapter;
    private StatusesListener listener;
    private View footer;
    private AtomicBoolean blockFireListenerOnPageEnd;

    public StatusesListView(Context context) {
        super(context);

        init();
    }

    public StatusesListView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public StatusesListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        footer = LayoutInflater.from(getContext()).inflate(R.layout.statuses_loading, null);
        addFooterView(footer);

        adapter = new StatusesAdapter(getContext());
        setAdapter(adapter);

        blockFireListenerOnPageEnd = new AtomicBoolean(false);

        setOnScrollListener(this);

        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
    }

    /**
     * Ustawienie słuchacza dla zdarzeń
     * @param listener obiekt słuchacza
     */
    public void setStatusesListener(StatusesListener listener) {
        this.listener = listener;
        if (adapter != null) {
            adapter.setStatusesListener(listener);
        }
    }

    /**
     * Czyszczenie zawartości adaptera
     */
    public void clear() {
        adapter.clear();
    }

    /**
     * Dodanie statusów na początku listy
     * @param statuses lista statusów
     */
    public void addStatusesAtBeginning(List<Status> statuses) {
        adapter.addStatusesAtBeginning(statuses);
    }

    /**
     * Dodanie statusów na końcu listy
     * @param statuses lista statusów
     */
    public void addStatuses(List<Status> statuses) {
        adapter.addStatuses(statuses);
        removeFooterView(footer);
    }

    /**
     * Pobranie zawartości listy
     * @return lista statusów
     */
    public List<Status> getStatuses() {
        return adapter.getStatuses();
    }

    /**
     * Pokazanie w stopce komórki z {@link android.widget.ProgressBar}
     * Dodatkowo funckja upewnia się że widoczna będzie tylko jedna stopka.
     */
    public void showFooter() {
        if (getFooterViewsCount() < 1) {
            addFooterView(footer);
        }
    }

    /**
     * Ukrycie stopki
     */
    public void hideFooter() {
        removeFooterView(footer);
    }

    public boolean isBlockFireListenerOnPageEnd() {
        return blockFireListenerOnPageEnd.get();
    }

    /**
     * Blokowanie wywołania zdarzenia przewinięcia do końca strony
     * @param value czy zdarzenie ma działać
     */
    public void setBlockFireListenerOnPageEnd(boolean value) {
        blockFireListenerOnPageEnd.set(value);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (getFooterViewsCount() >= 1) {
            log.trace("getFooterViewsCount() == 1");
            return;
        }

        if (adapter.getCount() > 0 && getChildAt(adapter.getCount()) != null) {
            if (getChildAt(adapter.getCount()).getBottom() < getHeight()) {
                return;
            }
        }

        if (!blockFireListenerOnPageEnd.get() && (firstVisibleItem + visibleItemCount >= totalItemCount)) {
            if (listener != null) {
                listener.onPageEnd();
            }
        }
    }
}
