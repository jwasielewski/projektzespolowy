package pk.birdie.statuseslistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import pk.birdie.R;
import pk.birdie.configuration.Configuration;
import pk.birdie.utilities.BirdieLinkMovementMethod;
import pk.birdie.utilities.DateFormatter;
import pk.birdie.utilities.RoundedTransformation;
import pk.birdie.utilities.StatusTextFormatter;
import twitter4j.Status;

/**
 * Adapter renderujący obiekty klasy {@link twitter4j.Status}
 */
public class StatusesAdapter extends BaseAdapter {
    private static final Logger log = LoggerFactory.getLogger(StatusesAdapter.class);

    private Context context;
    private List<Status> statuses;
    private StatusesListener listener;

    public StatusesAdapter(Context context) {
        this.context = context;
        statuses = new ArrayList<>();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {
        return statuses != null ? statuses.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return statuses != null ? statuses.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.status_view, null);
            holder.photo = (ImageView) convertView.findViewById(R.id.status_view_photo);
            holder.name = (TextView) convertView.findViewById(R.id.status_view_name);
            holder.nick = (TextView) convertView.findViewById(R.id.status_view_nick);
            holder.content = (TextView) convertView.findViewById(R.id.status_view_content);
            holder.date = (TextView) convertView.findViewById(R.id.status_view_date);
            holder.share = (TextView) convertView.findViewById(R.id.status_view_share);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context).load(statuses.get(position).getUser().getBiggerProfileImageURL())
                .transform(new RoundedTransformation(8, 2)).noFade().into(holder.photo);

        holder.name.setText(statuses.get(position).getUser().getName());
        holder.nick.setText("@" + statuses.get(position).getUser().getScreenName());
        holder.date.setText(DateFormatter.dateToString(statuses.get(position).getCreatedAt()));

        holder.content.setLinksClickable(true);
        holder.content.setText(StatusTextFormatter.format(statuses.get(position)));
        holder.content.setMovementMethod(BirdieLinkMovementMethod.getInstance(context));

        holder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onUserNameClick(statuses.get(position).getUser());
                }
            }
        });

        holder.photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onUserNameClick(statuses.get(position).getUser());
                }
            }
        });


        if (Configuration.getCurrentLoggedUserId() == statuses.get(position).getUser().getId()) {
            holder.share.setVisibility(View.INVISIBLE);
           // ((LinearLayout) holder.share.getParent()).setVisibility(View.GONE);
            holder.share.setOnClickListener(null);
        } else {
            holder.share.setVisibility(View.VISIBLE);
            holder.share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onStatusClick(statuses.get(position));
                    }
                }
            });
        }

        return convertView;
    }

    /**
     * Czyszczenie zawartości adaptera
     */
    public void clear() {
        if (statuses != null) {
            statuses.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * Dodanie statusów na początku wewnętrznej listy
     * @param statuses lista statusów
     */
    public void addStatusesAtBeginning(List<Status> statuses) {
        if (this.statuses.size() == 0) {
            return;
        }

        Status firstStatus = this.statuses.get(0);
        for (int i = statuses.size() - 1; i >= 0; --i) {
            if (statuses.get(i).getCreatedAt().compareTo(firstStatus.getCreatedAt()) == 1) {
                this.statuses.add(0, statuses.get(i));
            }
        }

        notifyDataSetChanged();
    }

    /**
     * Dodanie statusów na końcu listy
     *
     */
    public void addStatuses(List<Status> value) {
        // czasem zdarza się tak, że ostatni aktualnie wyświetlany status jest taki sam jak pierwszy status
        // z kolejnej strony, dlatego należy go usunąć
        if ((getCount() > 0) && (statuses.get(getCount() - 1).getId() == value.get(0).getId())) {
            value.remove(0);
        }

        statuses.addAll(value);

        notifyDataSetChanged();
    }

    /**
     * Pobranie zawartości adaptera
     * @return lista statusów
     */
    public List<Status> getStatuses() {
        return statuses;
    }

    /**
     * Ustawienie słuchacza dla zdarzeń listy
     * @param listener obiekt słuchacza
     */
    public void setStatusesListener(StatusesListener listener) {
        this.listener = listener;
    }

    private static class ViewHolder {
        public ImageView photo;
        public TextView name;
        public TextView nick;
        public TextView content;
        public TextView date;
        public TextView share;
    }
}
